﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsApp1.Classes_Basicas;

namespace WindowsFormsApp1.Classes
{
    class usuariodatabase
    {
        public bool Logar(string nome, string idade)
        {
            string script =
                @"select * 
                    from tb_usuario
                   where nm_usuario = @nm_usuario
                     and vl_idade   = @vl_idade";

            List<MySqlParameter> parameter = new List<MySqlParameter>();
            parameter.Add(new MySqlParameter("nm_usuario", nome));
            parameter.Add(new MySqlParameter("vl_idade", idade));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parameter);

            if (reader.Read())
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}

