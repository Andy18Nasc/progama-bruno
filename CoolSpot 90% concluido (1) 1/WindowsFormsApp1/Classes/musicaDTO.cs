﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1.Classes
{
    public class musicaDTO
    {
        public int musica { get; set; }
        public string nome { get; set; }
        public string autor { get; set; }
        public string genero { get; set; }
    }
}
