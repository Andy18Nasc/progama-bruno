﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp1.Classes;

namespace WindowsFormsApp1
{
    public partial class Adicionar_musica : Form
    {
        public static Musicas Atual;

        validacoes v = new validacoes();
        public Adicionar_musica()
        {
            InitializeComponent();
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                
                musicaDTO musica = new musicaDTO();
                musica.nome = txtnome.Text;
                musica.autor = txtautor.Text;
                musica.genero = txtgenero.Text;


                musicabusiness business = new musicabusiness();
                business.Salvar(musica);

                EnviarMensagem("Musica salva com sucesso.");
            }
            catch (ArgumentException ex)
            {
                EnviarMensagemErro(ex.Message);
            }
            catch (Exception ex)
            {
                EnviarMensagemErro("error: " + ex.Message);
            }

        }

        private void EnviarMensagem(string sonic)
        {
            MessageBox.Show(sonic, "Nsf Jam",
                      MessageBoxButtons.OK,
                      MessageBoxIcon.Information);
        }
        musicaDTO musica;
        internal void LoadScreen(musicaDTO musica)
        {
            this.musica = musica;           
            txtnome.Text = musica.nome;
            txtautor.Text = musica.autor;
            txtgenero.Text = musica.genero;
        }

        private void EnviarMensagemErro(string tails)
        {
            MessageBox.Show(tails, "Nsf Jam",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Error);
        }        

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void Adicionar_musica_Load(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog dialog = new OpenFileDialog();
                DialogResult r = ShowDialog();
                if (r == DialogResult.OK)
                {
                    txtcaminho.Text = dialog.FileName;
                }
            }
            catch (ArgumentException ex)
            {

                EnviarMensagemErro(ex.Message);
            }
           
        }

        private void name_keypress(object sender, KeyPressEventArgs e)
        {
            v.soletras(e);
        }

        private void autor_keypress(object sender, KeyPressEventArgs e)
        {
            v.soletras(e);
        }

        private void genero_keypress(object sender, KeyPressEventArgs e)
        {
            v.soletras(e);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Lista tela = new Lista();
            tela.Show();
            Hide();
        }
    }
}
