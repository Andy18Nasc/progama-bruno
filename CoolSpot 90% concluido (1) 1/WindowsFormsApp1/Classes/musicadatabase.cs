﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsApp1.Classes_Basicas;

namespace WindowsFormsApp1.Classes
{
    class musicadatabase
    {
        public int Salvar(musicaDTO musica)
        {
            string script =
            @"INSERT INTO tb_musica 
            (
	            nm_nome,
	            nm_autor,	           
	            ds_genero,
            )
            VALUES
            (
	            @nm_nome,
	            @nm_autor,	            
	            @ds_genero,
            )";


            List<MySqlParameter> parametros = new List<MySqlParameter>();
            parametros.Add(new MySqlParameter("nm_nome", musica.nome));
            parametros.Add(new MySqlParameter("nm_autor", musica.autor));
            parametros.Add(new MySqlParameter("ds_genero", musica.genero));

            Database db = new Database();
            int pk = db.ExecuteInsertScriptWithPk(script, parametros);
            return pk;

            }
            public void Alterar(musicaDTO musica)
            {
                string script =
                @"UPDATE tb_musica 
                 SET nm_nome  = @nm_nome,
	                 nm_autor   = @nm_autor,
	                 dt_data   = @dt_data,
	                 ds_genero = @ds_genero,	                 
               WHERE id_musica = @id_musica";

                List<MySqlParameter> parms = new List<MySqlParameter>();
                parms.Add(new MySqlParameter("id_musica", musica.musica));
                parms.Add(new MySqlParameter("nm_musica", musica.nome));
                parms.Add(new MySqlParameter("ds_banda", musica.autor));
                parms.Add(new MySqlParameter("ds_genero", musica.genero));

                Database db = new Database();
                db.ExecuteInsertScript(script, parms);
            }

            public void Remover(int id)
            {
                string script =
                @"DELETE FROM tb_musica WHERE id_musica = @id_musica";

                List<MySqlParameter> parms = new List<MySqlParameter>();
                parms.Add(new MySqlParameter("id_musica", id));

                Database db = new Database();
                db.ExecuteInsertScript(script, parms);
            }

            public List<musicaDTO> Consultar(string musica)
            {
                string script =
                @"SELECT * 
                FROM tb_musica 
               WHERE nm_musica like @nm_musica";

                List<MySqlParameter> parms = new List<MySqlParameter>();
                parms.Add(new MySqlParameter("nm_musica", "%" + musica + "%"));
               

                Database db = new Database();
                MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

                List<musicaDTO> musicas = new List<musicaDTO>();

                while (reader.Read())
                {
                    musicaDTO novaMusica = new musicaDTO();
                    novaMusica.musica = reader.GetInt32("id_musica");
                    novaMusica.nome = reader.GetString("nm_nome");
                    novaMusica.autor = reader.GetString("ds_autor");
                    novaMusica.genero = reader.GetString("ds_genero");

                    musicas.Add(novaMusica);
                }
                reader.Close();

                return musicas;

            }
        }
    }

