﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp1.Classes;

namespace WindowsFormsApp1
{
    public partial class Musicas : Form
    {
        public Musicas()
        {
            InitializeComponent();
        }

        private void Musicas_Load(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            btnplay.Visible = false;
            btnpause.Visible = true;
         //  mediaPlayer.Ctlcontrols.play();
         //  timer.Start();
        }

        private void listview1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            
        }

        private void gvMusicas_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 3)
            {
                musicaDTO musica = dgvmusicas.CurrentRow.DataBoundItem as musicaDTO;
            }

            if (e.ColumnIndex == 4)
            {
                musicaDTO musica = dgvmusicas.CurrentRow.DataBoundItem as musicaDTO;
                Adicionar_musica Tela = new Adicionar_musica();
                Tela.LoadScreen(musica);

                Adicionar_musica.Atual.OpenScreen(Tela);
            }

            if (e.ColumnIndex == 5)
            {
                musicaDTO musica = dgvmusicas.CurrentRow.DataBoundItem as musicaDTO;

                DialogResult r = MessageBox.Show("Deseja excluir a música?", ":P ",
                                    MessageBoxButtons.YesNo,
                                    MessageBoxIcon.Question);

                if (r == DialogResult.Yes)
                {
                    musicabusiness business = new musicabusiness();
                    business.Remover(musica.musica);


                }
            }
        }

        private void OpenScreen(Adicionar_musica Control)
        {
            if (dgvmusicas.Controls.Count == 1)
                dgvmusicas.Controls.RemoveAt(0);
            dgvmusicas.Controls.Add(Control);
        }

      

        private void dgvmusicas_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void btnbuscaai_Click(object sender, EventArgs e)
        {
            CarregarGrid();
        }
        public void CarregarGrid()
        {
            try
            {
                string musica = txtmusica.Text;

                musicabusiness business = new musicabusiness();
                List<musicaDTO> musicas = business.Consultar(musica);

                dgvmusicas.AutoGenerateColumns = false;
                dgvmusicas.DataSource = musicas;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreu um erro ao salvar a música: " + ex.Message, "Nsf Jam",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Error);
            }
        }

        private void txtmusica_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnpause_Click(object sender, EventArgs e)
        {
            btnplay.Visible = true;
            btnpause.Visible = false;
          //  mediaPlayer.Ctlcontrols.pause();
          //  timer.Stop();
        }

        private void fileSystemWatcher1_Changed(object sender, System.IO.FileSystemEventArgs e)
        {
            
        }
    }
}

