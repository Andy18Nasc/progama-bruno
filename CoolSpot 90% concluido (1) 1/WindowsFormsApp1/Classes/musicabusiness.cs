﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1.Classes
{
    class musicabusiness
    {

            musicadatabase db = new musicadatabase();

            public int Salvar(musicaDTO musica)
            {
                if (musica.nome == string.Empty)
                {
                    throw new ArgumentException("Música é obrigatória.");
                }

                return db.Salvar(musica);
            }

            public void Alterar(musicaDTO musica)
            {
                if (musica.nome == string.Empty)
                {
                    throw new ArgumentException("o nome da música é obrigatória.");
                }

                db.Alterar(musica);
            }

            public void Remover(int musica)
            {
                db.Remover(musica);
            }

            public List<musicaDTO> Consultar(string musica)
            {
                if (musica == "Todos")
                {
                    musica = string.Empty;
                }

                return db.Consultar(musica);
            }
        }
    
}