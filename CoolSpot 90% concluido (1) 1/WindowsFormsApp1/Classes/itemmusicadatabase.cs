﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsApp1.Classes_Basicas;

namespace WindowsFormsApp1.Classes
{
    class itemmusicadatabase
    {
        public int Salvar(itemmusicaDTO dto)
        {
            string script = @"INSERT INTO tb_itemmusica (id_musica, id_usuario) VALUES (@id_musica, @id_usuario)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_musica", dto.Idmusica));
            parms.Add(new MySqlParameter("id_usuario", dto.Idusuario));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);

        }
    }
}